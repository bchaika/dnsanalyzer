package SimpleTests;

import DNSAnalyzer.main.Main;
import org.junit.Test;

public class FirstSimpleTest {

    @Test
    public void check3FilesAndFindOneAnomaly() {
        java.io.ByteArrayOutputStream out = new java.io.ByteArrayOutputStream();
        System.setOut(new java.io.PrintStream(out));

        Main.main(new String[]{"-t", "2", "-p", "src\\test\\resources\\simple_tests\\first_simple_test\\"});
        assert out.toString().toLowerCase().split("anomaly").length == 2;
    }
}
