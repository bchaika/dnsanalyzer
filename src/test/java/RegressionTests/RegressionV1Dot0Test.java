package RegressionTests;

import DNSAnalyzer.main.Main;
import org.apache.commons.io.FileDeleteStrategy;
import org.junit.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Random;

public class RegressionV1Dot0Test {

    private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static final String PATH = "src\\test\\resources\\regression_tests\\regression_v_1.0_test\\";
    private static final int START_COUNT = 100;
    private static final int ADDED_COUNT = 1000;

    private int i = 0;

    @Test
    public void durabilityTest() {
        cleanDirectory();
        for (int i = 0; i < START_COUNT; i++) {
            createRandomFile();
        }
        new Thread(() -> {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            for (int i = 0; i < ADDED_COUNT; i++) {
                createRandomFile();
            }
        }).start();
        Main.main(new String[]{"-t", "10", "-p", PATH});
        assert i == START_COUNT + ADDED_COUNT;
    }

    private void cleanDirectory() {
        System.out.println("empties test directory");
        File directory = new File(PATH);
        File[] files = directory.listFiles();
        if (files != null) {
            for (File file : files) {
                try {
                    FileDeleteStrategy.FORCE.delete(file);
                } catch (IOException e) {
                    System.out.println("can't delete file : " + file.getName());
                }
            }
        }
    }

    private void createRandomFile() {
        File file = new File(PATH, i + ".log");
        StringBuilder builder = new StringBuilder();
        Random random = new SecureRandom();
        builder.append(i).append("\n");
        builder.append(random.nextInt(100)).append("\n");
        builder.append(AB.charAt(random.nextInt(AB.length()))).
                append(AB.charAt(random.nextInt(AB.length()))).append("\n");
        int count = random.nextInt(4);
        for (int j = 0; j < count; j++) {
            builder.append(random.nextInt(10000)).append("\n");
        }
        try (FileOutputStream stream = new FileOutputStream(file);
             OutputStreamWriter streamWriter = new OutputStreamWriter(
                     stream, StandardCharsets.UTF_8);
             Writer writer = new BufferedWriter(streamWriter)) {

                    writer.write(builder.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
        i++;
    }

}
