package DNSAnalyzer.main;

import DNSAnalyzer.solution.AnalyzeManager;
import DNSAnalyzer.solution.LogQueueManager;
import org.apache.commons.cli.*;

import java.io.File;
import java.util.Date;

public class Main {

    private static int numberOfThreads = Runtime.getRuntime().availableProcessors() * 2;
    private static String pathSourceFiles = System.getProperty("user.dir");

    public static void main (String [] args) {
        System.out.println("Preparing...");
        Date preparingStartTime = new Date();
        setupCLI(args);
        prepareLogFiles();
        AnalyzeManager.getInstance().analyze(numberOfThreads, pathSourceFiles, preparingStartTime);
    }

    private static void setupCLI(String[] args) {
        Options gnuOptions = new Options();
        gnuOptions.addOption("t", "threads", true, "number of threads")
                .addOption("p", "path", true, "path to source folder");
        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(gnuOptions, args);
            if (cmd.hasOption("t")) {
                numberOfThreads = Integer.parseInt(cmd.getOptionValue("t"));
            }
            if (cmd.hasOption("p")) {
                pathSourceFiles = cmd.getOptionValue("p");
            }
        } catch (ParseException e) {
            System.out.println("something goes wrong with parsing your options");
            e.printStackTrace();
        }
    }

    private static void prepareLogFiles() {
        LogQueueManager queueManager = LogQueueManager.getInstance();
        File folder = new File(pathSourceFiles);
        if (folder.exists() && folder.isDirectory()) {
            File[] files = folder.listFiles();
            if (files !=  null) {
                for (final File fileEntry : files) {
                    if (fileEntry.isFile() && fileEntry.getName().contains(".log")) {
                        queueManager.addFileLog(fileEntry);
                    }
                }
            }
        } else {
            System.out.println(pathSourceFiles + " is not exist or not a folder");
        }
    }



}
