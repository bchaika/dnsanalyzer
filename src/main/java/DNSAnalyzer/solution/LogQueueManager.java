package DNSAnalyzer.solution;

import java.io.File;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class LogQueueManager {

    private static LogQueueManager queueManager;

    private ReadWriteLock rwLock = new ReentrantReadWriteLock();

    private List<Log> queueList = new ArrayList<>();

    public static LogQueueManager getInstance() {
        if (queueManager == null) {
            queueManager = new LogQueueManager();
        }
        return queueManager;
    }

    public void addFileLog (File file) {
        Lock wLock = rwLock.writeLock();
        wLock.lock();
        try {
            queueList.add(new Log(file));
        } finally {
            wLock.unlock();
        }
    }

    void sortQuery() {
        queueList.sort(Comparator.comparingLong(Log::getTimestamp));
    }

    boolean isEmpty() {
        return queueList.isEmpty();
    }

    Log getFirstLog() {
        Lock rLock = rwLock.readLock();
        rLock.lock();
        Log firstLog;
        try {
            firstLog = queueList.get(0);
        } finally {
            rLock.unlock();
        }
        Lock wLock = rwLock.writeLock();
        wLock.lock();
        try {
            queueList.remove(0);
        } finally {
            wLock.unlock();
        }
        return firstLog;
    }

    void end() {
        queueManager = null;
    }
}
