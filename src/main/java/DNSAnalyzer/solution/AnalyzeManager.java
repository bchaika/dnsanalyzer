package DNSAnalyzer.solution;

import java.nio.file.*;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class AnalyzeManager {

    private static AnalyzeManager analyzer;

    private static final int MAX_DIFFERENCE = 1000;
    private static final int MAX_ATTEMPTS = 10;
    private static final int SEND_INTERVAL = 1;
    private static final int ATTEMPT_INTERVAL = 50;

    private ConcurrentMap<String, List<Long>> domainIPDataMap = new ConcurrentHashMap<>();

    public static AnalyzeManager getInstance() {
        if (analyzer == null) {
            analyzer = new AnalyzeManager();
        }
        return analyzer;
    }

    public void analyze(int numberOfThreads, String pathSourceFiles, Date preparingStartTime) {
        LogQueueManager queueManager = LogQueueManager.getInstance();
        queueManager.sortQuery();
        FileWatcher fileWatcher = new FileWatcher();

        System.out.println(String.format("Preparing is done in %d ms, analyzing... \n",
                new Date().getTime() - preparingStartTime.getTime()));

        Date analyzingStartTime = new Date();
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(numberOfThreads + 1);
        executor.submit(() -> fileWatcher.watchDirectoryPath(Paths.get(pathSourceFiles)));
        while (true) {

            int attempts = 0;
            while (queueManager.isEmpty() && attempts < MAX_ATTEMPTS) {
                sleep(ATTEMPT_INTERVAL);
                if (!queueManager.isEmpty()) {
                    break;
                }
                attempts++;
            }
            if (queueManager.isEmpty() && attempts >= MAX_ATTEMPTS) {
                break;
            }

            executor.submit(() -> {
                Log log = queueManager.getFirstLog();
                System.out.println("analyzing : " + log.getFileName());
                if (domainIPDataMap.containsKey(log.getDomain())) {
                    if (isAnomalyInIPLists(domainIPDataMap.get(log.getDomain()), log.getServersIP())) {
                        registerAnomaly(log);
                    }
                } else {
                    domainIPDataMap.put(log.getDomain(), log.getServersIP());
                }
            });

            sleep(SEND_INTERVAL);

        }
        System.out.println(String.format("Analyzing is done in %d ms!!! \n",
                new Date().getTime() - analyzingStartTime.getTime()));
        end(executor);
    }

    private void sleep (long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private boolean isAnomalyInIPLists(List<Long> baseList, List<Long> currentList) {
        for (Long currentIP : currentList) {
            long difference = Long.MAX_VALUE;
            for (Long baseIP : baseList) {
                if (Math.abs(baseIP - currentIP) < difference) {
                    difference = Math.abs(baseIP - currentIP);
                }
            }
            if (difference > MAX_DIFFERENCE) {
                return true;
            }
        }
        return false;
    }

    private void registerAnomaly(Log log) {
        System.out.println("Anomaly found in " + log.getFileName() + " for domain " + log.getDomain());
    }

    private void end(ThreadPoolExecutor executor) {
        executor.shutdownNow();
        LogQueueManager.getInstance().end();
        analyzer = null;
    }

}
