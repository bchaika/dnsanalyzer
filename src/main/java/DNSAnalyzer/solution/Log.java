package DNSAnalyzer.solution;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Log {
    private String fileName;
    private long timestamp;
    private int clientIP;
    private String domain;
    private List<Long> serversIP = new ArrayList<>();

    Log(File file) {
        try {
            fileName = file.getName();
            Scanner scanner = new Scanner(file);
            String line;
            int i = 0;
            while(scanner.hasNext()) {
                line = scanner.nextLine();
                if (i == 0) {
                    timestamp = Long.valueOf(line);
                } else if (i == 1) {
                    clientIP = Integer.valueOf(line);
                } else if (i == 2) {
                    domain = line;
                } else if (i >= 3) {
                    serversIP.add(Long.valueOf(line));
                }
                i++;
            }
        } catch (FileNotFoundException e) {
            System.out.println("cant find " + file.getName());
            e.printStackTrace();
        }
    }

    String getFileName() {
        return fileName;
    }

    long getTimestamp() {
        return timestamp;
    }

    int getClientIP() {
        return clientIP;
    }

    String getDomain() {
        return domain;
    }

    List<Long> getServersIP() {
        return serversIP;
    }
}
