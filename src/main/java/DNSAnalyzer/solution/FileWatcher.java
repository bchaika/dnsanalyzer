package DNSAnalyzer.solution;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;

import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardWatchEventKinds.*;

class FileWatcher {

    void watchDirectoryPath(Path path) {
        try {
            Boolean isFolder = (Boolean) Files.getAttribute(path,
                    "basic:isDirectory", NOFOLLOW_LINKS);
            if (!isFolder) {
                throw new IllegalArgumentException("Path: " + path
                        + " is not a folder");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileSystem fs = path.getFileSystem();

        try (WatchService service = fs.newWatchService()){
            path.register(service, ENTRY_CREATE);
            while (true) {
                WatchKey key;
                key = service.take();
                for (WatchEvent<?> event: key.pollEvents()) {
                    WatchEvent.Kind<?> kind = event.kind();
                    if (kind == ENTRY_CREATE) {
                        WatchEvent<Path> ev = (WatchEvent<Path>)event;
                        Path filename = ev.context();
                        LogQueueManager.getInstance().addFileLog(
                                new File(path + File.separator + filename.getFileName().toString()));
                    }
                }
                boolean valid = key.reset();
                if (!valid) {
                    break;
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
        }
    }

}
